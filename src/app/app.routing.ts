import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Componentes

import { ReporteComponent } from './components/reporte/reporte.component';
import { layoutIndexComponent } from './components/templates/index.component';
import { loginComponent } from './components/login/login.component';
import { logoutComponent } from './components/login/logout.component';
import { homeComponent } from './components/home/home.component';

const appRoutes: Routes = [
    { path: '', component: loginComponent },
    { path: 'home', component: homeComponent },
    { path: 'logout', component: logoutComponent }

];

export const  appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

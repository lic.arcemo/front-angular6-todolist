import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';  // replaces previous Http service
import { routing, appRoutingProviders } from './app.routing';

//components
import { AppComponent } from './app.component';
import { ReporteComponent } from './components/reporte/reporte.component';
import { layoutIndexComponent } from './components/templates/index.component';
import { loginComponent } from './components/login/login.component';
import { logoutComponent } from './components/login/logout.component';
import { homeComponent } from './components/home/home.component';
import { menuComponent } from './components/menu/menu.component';
import { headertopComponent } from './components/headertop/headertop.component';

@NgModule({
  declarations: [
    AppComponent,
    ReporteComponent,
    layoutIndexComponent,
    loginComponent,
    homeComponent,
    headertopComponent,
    menuComponent,
    logoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }

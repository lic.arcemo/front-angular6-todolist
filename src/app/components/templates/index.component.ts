import { Component } from '@angular/core';

@Component({
  selector: 'layoutIndex',
  templateUrl: './index.component.html'
})

export class layoutIndexComponent {
  public nameLayout;

  constructor(){
    this.nameLayout= "Index layout";
  }
}

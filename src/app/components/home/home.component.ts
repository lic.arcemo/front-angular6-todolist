import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { Response, Headers, RequestOptions,Http} from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../models/user';
import { Tarea } from '../../models/tarea';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: [
    "home.css"
  ]
})



export class homeComponent {
  public App = "App";
  public user = <User>Object;
  public options;
  public Activities = [];
  public tarea = <Tarea>Object;

 //  interface Tarea {
 //    _id: string;
 //  }
 //
 // public tarea: Object<Tarea>;

  constructor(private router: Router, private http:HttpClient){

    if(localStorage.getItem("user")===null) {
      this.router.navigate(['/']);
    }
    else{
      this.user = JSON.parse(localStorage.getItem("user"));
      console.log(this.user);
      this.getActivities();
    }


  }

  getActivities() {
    this.http.get<any>( 'http://localhost:8080/activities',{ headers: {"x-access-token": this.user.token} })
    .toPromise()
    .then((res) => {
      this.Activities = res;
      console.log(res);
      this.tarea= {};
    })
    .catch(err=> { console.log(err) });
  }

  editTarea(){
    this.http.post( 'http://localhost:8080/activities/'+this.tarea._id,this.tarea,{ headers: {"x-access-token": this.user.token} })
    .toPromise()
    .then((res) => {
      this.tarea={};
      $('#modal').modal("hide");
    })
    .catch(err=> { console.log(err) });
  }

  deleteTarea(event, index, tarea) {
    this.tarea=tarea;
    if(window.confirm('Seguro que deseas eliminar la tarea?')) {
      this.http.delete( 'http://localhost:8080/activities/'+this.tarea._id,{ headers: {"x-access-token": this.user.token} })
      .toPromise()
      .then((res) => {
        this.Activities.splice(index, 1);
      })
      .catch(err=> { console.log(err) });
      } else {
        return false;
      }
    }

  crearTarea(){
    this.tarea.actor = this.user.user_id;
    console.log(this.tarea);
    this.http.post( 'http://localhost:8080/activities/',this.tarea,{ headers: {"x-access-token": this.user.token} })
    .toPromise()
    .then((res) => {
      this.tarea={};
      this.Activities.push(res);
      $('#modalNew').modal("hide");
    })
    .catch(err=> { console.log(err) });
  }

  abrirModal(idModal,tipo, tarea) {
    $('#'+idModal).modal("show");
    if (tipo==='editar'){
      console.log("editar ds");
      this.tarea = tarea;
    }
  }


}

import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions,Http} from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from "@angular/router";


const headers = new Headers({ 'Content-Type': 'application/json' });
const options = new RequestOptions({ headers: headers });

@Injectable()

@Component({
  selector: 'logout',
  templateUrl: './logout.component.html'
})

export class logoutComponent {
  public title;
  public user = {};

  constructor(private router: Router){
    this.logout();
  }

  logout() {
    localStorage.removeItem("user");
    console.log(localStorage.getItem("user"));
      this.router.navigate(['/']);
  }


}

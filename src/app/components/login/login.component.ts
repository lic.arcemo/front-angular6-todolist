import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions,Http} from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from "@angular/router";


const headers = new Headers({ 'Content-Type': 'application/json' });
const options = new RequestOptions({ headers: headers });

@Injectable()

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ["login.css"]
})

export class loginComponent {
  public title;
  public user = {};
  public respuesta = <any>Object;

  constructor( private http:HttpClient, private router: Router ){

    this.title= "Login";

  }

  login() {

    this.http.post( 'http://localhost:8080/users/login', this.user)
    .toPromise()
    .then((res) => {
      this.respuesta = res;
      console.log(this.respuesta);
      if(this.respuesta.auth){
        localStorage.setItem("user", JSON.stringify(res));
        this.router.navigate(['/home']);
      }
      else{
        alert(this.respuesta.message);
      }
    })
    .catch(err=> {
      console.log(err)
    });
  }

}
